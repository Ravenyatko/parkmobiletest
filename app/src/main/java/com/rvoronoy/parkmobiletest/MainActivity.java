package com.rvoronoy.parkmobiletest;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.rvoronoy.parkmobiletest.ui.DetailsNavigator;
import com.rvoronoy.parkmobiletest.ui.fragments.VehicleDetailsFragment;
import com.rvoronoy.parkmobiletest.ui.fragments.VehicleListFragment;
import com.rvoronoy.parkmobiletest.util.Consts;
import com.rvoronoy.parkmobiletest.viewmodel.MainActivityViewModel;

public class MainActivity extends AppCompatActivity implements DetailsNavigator {


    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSRLayout;
    @Nullable
    @BindView(R.id.container_details)
    View mDetailsContainer;

    private MainActivityViewModel mMainActivityViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        showUI();
    }

    private void init() {
        ButterKnife.bind(this);
        mMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mMainActivityViewModel.getVehicleList().observe(this, vehicles -> mSRLayout.setRefreshing(false));
        mSRLayout.setOnRefreshListener(this::refreshData);
    }

    private void showUI() {
        showListFragment();
        if (mDetailsContainer != null) {
            showDetailsFragment(R.id.container_details, null);
        }
        if (mMainActivityViewModel.getCurrentVehicle() != Consts.DEFAULT_VEHICLE_ID) {
            showDetailsFragment(mMainActivityViewModel.getCurrentVehicle());
        }
    }

    private void showListFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new VehicleListFragment())
                .commit();
    }

    private void showDetailsFragment(int containerId, Integer vehicleId) {
        getSupportFragmentManager().beginTransaction()
                .add(containerId, VehicleDetailsFragment.newInstance(vehicleId))
                .addToBackStack("details")
                .commit();
    }

    private void refreshData() {
        mMainActivityViewModel.refreshData();
    }

    @Override
    public void showDetailsFragment(int vehicleId) {
        mMainActivityViewModel.setCurrentVehicle(vehicleId);
        if (mDetailsContainer != null) {
            showDetailsFragment(R.id.container_details, vehicleId);
        } else {
            showDetailsFragment(R.id.container, vehicleId);
        }
    }
}
