package com.rvoronoy.parkmobiletest;

import android.app.Application;
import com.rvoronoy.parkmobiletest.di.AppComponent;
import com.rvoronoy.parkmobiletest.di.DaggerAppComponent;

public class PMTestApp extends Application {

    private static AppComponent mAppComponent;
    private static PMTestApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.create();
        mInstance = this;
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }

    public static PMTestApp getInstance() {
        return mInstance;
    }
}
