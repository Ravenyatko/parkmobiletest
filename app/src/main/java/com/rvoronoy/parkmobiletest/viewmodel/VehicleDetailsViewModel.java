package com.rvoronoy.parkmobiletest.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.rvoronoy.parkmobiletest.PMTestApp;
import com.rvoronoy.parkmobiletest.data.DataRepository;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;

import javax.inject.Inject;

public class VehicleDetailsViewModel extends ViewModel {

    @Inject
    DataRepository mDataRepo;

    public VehicleDetailsViewModel() {
        PMTestApp.getAppComponent().inject(this);
    }

    private LiveData<Vehicle> mVehicle;

    public LiveData<Vehicle> getVehicleDetails(int vehicleId) {
        if (mVehicle == null) {
            mVehicle = mDataRepo.subscribeToVehicle(vehicleId);
        }
        return mVehicle;
    }
}
