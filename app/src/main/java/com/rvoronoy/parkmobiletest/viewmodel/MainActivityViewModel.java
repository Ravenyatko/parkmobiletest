package com.rvoronoy.parkmobiletest.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.rvoronoy.parkmobiletest.PMTestApp;
import com.rvoronoy.parkmobiletest.data.DataRepository;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.util.Consts;

import javax.inject.Inject;
import java.util.List;

public class MainActivityViewModel extends ViewModel {

    @Inject
    DataRepository mDataRepo;

    private int mCurrentVehicle = Consts.DEFAULT_VEHICLE_ID;

    public MainActivityViewModel() {
        PMTestApp.getAppComponent().inject(this);
    }

    private LiveData<List<Vehicle>> mVehicleList;

    public LiveData<List<Vehicle>> getVehicleList() {
        if (mVehicleList == null) {
            mVehicleList = mDataRepo.subscribeToVehicles();
        }
        return mVehicleList;
    }

    public void refreshData() {
        mDataRepo.refreshVehicles();
    }

    public int getCurrentVehicle() {
        return mCurrentVehicle;
    }

    public void setCurrentVehicle(int currentVehicle) {
        this.mCurrentVehicle = currentVehicle;
    }
}
