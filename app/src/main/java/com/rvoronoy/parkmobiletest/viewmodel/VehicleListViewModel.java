package com.rvoronoy.parkmobiletest.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.rvoronoy.parkmobiletest.PMTestApp;
import com.rvoronoy.parkmobiletest.data.DataRepository;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;

import javax.inject.Inject;
import java.util.List;

public class VehicleListViewModel extends ViewModel {

    @Inject
    DataRepository mDataRepo;

    public VehicleListViewModel() {
        PMTestApp.getAppComponent().inject(this);
    }

    private LiveData<List<Vehicle>> mVehicleList;

    public LiveData<List<Vehicle>> getVehicleList() {
        if (mVehicleList == null) {
            mVehicleList = mDataRepo.subscribeToVehicles();
        }
        return mVehicleList;
    }
}
