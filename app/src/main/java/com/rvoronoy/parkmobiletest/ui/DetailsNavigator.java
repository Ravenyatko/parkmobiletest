package com.rvoronoy.parkmobiletest.ui;

public interface DetailsNavigator {

    void showDetailsFragment(int vehicleId);
}
