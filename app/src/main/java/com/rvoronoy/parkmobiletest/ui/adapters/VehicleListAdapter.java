package com.rvoronoy.parkmobiletest.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.rvoronoy.parkmobiletest.R;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.ui.DetailsNavigator;
import com.rvoronoy.parkmobiletest.ui.fragments.VehicleListFragment;
import com.rvoronoy.parkmobiletest.ui.vh.VehicleVH;

import java.util.ArrayList;
import java.util.List;

public class VehicleListAdapter extends RecyclerView.Adapter<VehicleVH> {

    private List<Vehicle> mItems = new ArrayList<>();
    private DetailsNavigator mDetailsNavigator;

    public void setDetailsNavigator(DetailsNavigator detailsNavigator) {
        this.mDetailsNavigator = detailsNavigator;
    }

    @NonNull
    @Override
    public VehicleVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VehicleVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.li_vehicle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleVH holder, int position) {
        holder.bind(mItems.get(position));
        holder.itemView.setOnClickListener(v -> {
            if (mDetailsNavigator != null) {
                mDetailsNavigator.showDetailsFragment(mItems.get(position).getVehicleId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItems(List<Vehicle> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }
}
