package com.rvoronoy.parkmobiletest.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.rvoronoy.parkmobiletest.R;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.util.Consts;
import com.rvoronoy.parkmobiletest.viewmodel.VehicleDetailsViewModel;

import static com.rvoronoy.parkmobiletest.util.Consts.DEFAULT_VEHICLE_ID;
import static com.rvoronoy.parkmobiletest.util.Consts.KEY_VEHICLE_ID;

public class VehicleDetailsFragment extends Fragment {

    @BindView(R.id.empty_tv)
    TextView mEmptyTv;
    @BindView(R.id.vrn_tv)
    TextView mVrnTv;
    @BindView(R.id.country_tv)
    TextView mCountryTv;
    @BindView(R.id.color_tv)
    TextView mColorTv;
    @BindView(R.id.type_tv)
    TextView mTypeTv;
    @BindView(R.id.default_cb)
    CheckBox mDefaultCB;

    public static VehicleDetailsFragment newInstance(Integer vehicleId) {
        VehicleDetailsFragment fragment = new VehicleDetailsFragment();
        if (vehicleId != null) {
            Bundle args = new Bundle();
            args.putInt(Consts.KEY_VEHICLE_ID, vehicleId);
            fragment.setArguments(args);
        }
        return fragment;
    }

    private VehicleDetailsViewModel mVehicleDetailsViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehicleDetailsViewModel = ViewModelProviders.of(this).get(VehicleDetailsViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getVehicleFromArguments();
    }

    private void getVehicleFromArguments() {
        if (getActivity() == null) return;
        int vehicleId = getArguments() == null ? DEFAULT_VEHICLE_ID :
                getArguments().getInt(KEY_VEHICLE_ID, DEFAULT_VEHICLE_ID);
        loadData(vehicleId);
    }

    private void loadData(int vehicleId) {
        if (getActivity() == null) return;
        if (vehicleId == DEFAULT_VEHICLE_ID) {
            showEmptyData();
        } else {
            mVehicleDetailsViewModel.getVehicleDetails(vehicleId)
                    .observe(this, this::showVehicleData);
        }
    }

    private void showEmptyData() {
        mEmptyTv.setVisibility(View.VISIBLE);
    }

    private void showVehicleData(Vehicle vehicle) {
        if (vehicle == null) {
            showEmptyData();
            return;
        }
        mVrnTv.setText(vehicle.getVrn());
        mCountryTv.setText(vehicle.getCountry());
        mColorTv.setText(vehicle.getColor());
        mTypeTv.setText(vehicle.getType());
        mDefaultCB.setChecked(vehicle.isDefault());
    }

}
