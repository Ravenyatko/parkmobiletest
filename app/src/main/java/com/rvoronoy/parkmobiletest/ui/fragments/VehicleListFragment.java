package com.rvoronoy.parkmobiletest.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.rvoronoy.parkmobiletest.R;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.ui.DetailsNavigator;
import com.rvoronoy.parkmobiletest.ui.adapters.VehicleListAdapter;
import com.rvoronoy.parkmobiletest.viewmodel.VehicleListViewModel;

import java.util.List;

public class VehicleListFragment extends Fragment implements DetailsNavigator {

    @BindView(R.id.vehicles_rv)
    RecyclerView mVehiclesRV;

    private VehicleListViewModel mVehicleListViewModel;
    private VehicleListAdapter mAdapter = new VehicleListAdapter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehicleListViewModel = ViewModelProviders.of(this).get(VehicleListViewModel.class);
        mVehicleListViewModel.getVehicleList().observe(this, vehicles -> updateUI(vehicles));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        if (getActivity() == null) return;
        mVehiclesRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mVehiclesRV.setAdapter(mAdapter);
    }

    private void updateUI(List<Vehicle> vehicles) {
        if (mVehiclesRV != null) {
            mAdapter.setItems(vehicles);
        }
    }

    @Override
    public void showDetailsFragment(int vehicleId) {
        Activity activity = getActivity();
        if (activity instanceof DetailsNavigator) {
            ((DetailsNavigator) activity).showDetailsFragment(vehicleId);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.setDetailsNavigator(this);
    }

    @Override
    public void onStop() {
        mAdapter.setDetailsNavigator(null);
        super.onStop();
    }
}
