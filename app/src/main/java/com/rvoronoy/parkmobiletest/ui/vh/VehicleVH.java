package com.rvoronoy.parkmobiletest.ui.vh;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.rvoronoy.parkmobiletest.R;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;

public class VehicleVH extends RecyclerView.ViewHolder {

    @BindView(R.id.vehicle_name_tv)
    TextView vehicleNameTV;

    public VehicleVH(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(@NonNull Vehicle vehicle) {
        vehicleNameTV.setText(vehicle.getVrn());
    }
}
