package com.rvoronoy.parkmobiletest.di;

import android.content.Context;
import com.rvoronoy.parkmobiletest.data.db.DBManager;
import com.rvoronoy.parkmobiletest.data.db.PMDatabase;
import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {
 
    @Provides
    DBManager provideDatabaseManager(Context context) {
        return new DBManager(PMDatabase.getInstance(context));
    }
 
}