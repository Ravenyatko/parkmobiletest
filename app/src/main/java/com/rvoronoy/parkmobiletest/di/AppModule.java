package com.rvoronoy.parkmobiletest.di;

import android.content.Context;
import com.rvoronoy.parkmobiletest.PMTestApp;
import com.rvoronoy.parkmobiletest.data.DataRepository;
import com.rvoronoy.parkmobiletest.data.db.DBManager;
import com.rvoronoy.parkmobiletest.data.db.PMDatabase;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    Context provideAppContext() {
        return PMTestApp.getInstance();
    }

    @Provides
    DataRepository provideDataRepository() {
        return new DataRepository();
    }
}