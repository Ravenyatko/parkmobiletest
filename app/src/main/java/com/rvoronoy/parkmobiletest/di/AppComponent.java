package com.rvoronoy.parkmobiletest.di;

import com.rvoronoy.parkmobiletest.MainActivity;
import com.rvoronoy.parkmobiletest.data.DataRepository;
import com.rvoronoy.parkmobiletest.viewmodel.MainActivityViewModel;
import com.rvoronoy.parkmobiletest.viewmodel.VehicleDetailsViewModel;
import com.rvoronoy.parkmobiletest.viewmodel.VehicleListViewModel;
import dagger.Component;

@Component(modules = {AppModule.class, NetworkModule.class, StorageModule.class})
public interface AppComponent {

    void inject(DataRepository dataRepository);

    void inject(VehicleListViewModel vehicleListViewModel);

    void inject(MainActivityViewModel mainActivityViewModel);

    void inject(VehicleDetailsViewModel vehicleDetailsViewModel);
}
