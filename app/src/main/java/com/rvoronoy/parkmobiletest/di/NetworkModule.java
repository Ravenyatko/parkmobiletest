package com.rvoronoy.parkmobiletest.di;

import com.rvoronoy.parkmobiletest.data.network.NetworkManager;
import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {
 
    @Provides
    NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }
 
}