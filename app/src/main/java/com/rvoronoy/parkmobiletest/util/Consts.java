package com.rvoronoy.parkmobiletest.util;

public class Consts {

    public static final String API_BASE_URL = "http://private-6d86b9-vehicles5.apiary-mock.com/";
    public static final String KEY_VEHICLE_ID = "vehicle_id";
    public static final int DEFAULT_VEHICLE_ID = -1;

}
