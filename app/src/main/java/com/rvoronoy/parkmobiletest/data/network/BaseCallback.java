package com.rvoronoy.parkmobiletest.data.network;

import android.util.Log;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseCallback<T> implements Callback<T> {

    private final static String TAG = "NetworkResponse";

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            onResponse(response.body());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable throwable) {
        Log.e(TAG, throwable.getMessage());
    }

    public abstract void onResponse(T data);
}
