package com.rvoronoy.parkmobiletest.data.db.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleServerModel;

@Entity(tableName = "vehicles_table")
public class Vehicle {

    @PrimaryKey
    @ColumnInfo(name = "vehicleId")
    private int vehicleId;
    @ColumnInfo(name = "vrn")
    private String vrn;
    @ColumnInfo(name = "country")
    private String country;
    @ColumnInfo(name = "color")
    private String color;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "default")
    private boolean isDefault;

    public Vehicle() {
    }

    public Vehicle(VehicleServerModel serverModel) {
        this.vehicleId = serverModel.getVehicleId();
        this.vrn = serverModel.getVrn();
        this.country = serverModel.getCountry();
        this.color = serverModel.getColor();
        this.type = serverModel.getType();
        this.isDefault = serverModel.isDefault();
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVrn() {
        return vrn;
    }

    public void setVrn(String vrn) {
        this.vrn = vrn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }
}