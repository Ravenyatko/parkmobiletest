package com.rvoronoy.parkmobiletest.data;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import com.rvoronoy.parkmobiletest.PMTestApp;
import com.rvoronoy.parkmobiletest.data.db.DBManager;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.data.network.BaseCallback;
import com.rvoronoy.parkmobiletest.data.network.NetworkManager;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleResponse;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleServerModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class DataRepository {

    private static final String TAG = DataRepository.class.getSimpleName();

    @Inject
    NetworkManager mNetworkManager;
    @Inject
    DBManager mDBManager;

    private Disposable mSaveDisposable;

    public LiveData<List<Vehicle>> subscribeToVehicles() {
        return mDBManager.getVehicles();
    }

    public LiveData<Vehicle> subscribeToVehicle(int vehicleId) {
        return mDBManager.getVehicle(vehicleId);
    }

    public DataRepository() {
        PMTestApp.getAppComponent().inject(this);
    }

    public void refreshVehicles() {
        mNetworkManager.loadVehicleList(new BaseCallback<VehicleResponse>() {
            @Override
            public void onResponse(VehicleResponse data) {
                if (data != null && data.getVehicles() != null) {
                    storeVehiclesInDB(data.getVehicles());
                }
            }
        });
    }

    private void storeVehiclesInDB(@NonNull List<VehicleServerModel> items) {
        ArrayList<Vehicle> parsedData = new ArrayList<>();
        for (VehicleServerModel serverModel : items) {
            parsedData.add(new Vehicle(serverModel));
        }
        if (mSaveDisposable != null && !mSaveDisposable.isDisposed()) {
            mSaveDisposable.dispose();
        }
        mSaveDisposable = mDBManager.saveVehicles(parsedData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.d(TAG, "Saved successfully"));
    }

}
