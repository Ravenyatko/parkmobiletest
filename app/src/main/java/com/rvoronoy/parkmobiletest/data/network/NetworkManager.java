package com.rvoronoy.parkmobiletest.data.network;

import com.rvoronoy.parkmobiletest.data.network.model.VehicleResponse;
import com.rvoronoy.parkmobiletest.util.Consts;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkManager {
    private VehicleService mVehicleService;

    public NetworkManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Consts.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mVehicleService = retrofit.create(VehicleService.class);
    }

    public void loadVehicleList(BaseCallback<VehicleResponse> callback) {
        Call<VehicleResponse> vehicleResponseCall = mVehicleService.getVehiclesList();
        vehicleResponseCall.enqueue(callback);
    }
}
