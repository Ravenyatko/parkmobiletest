package com.rvoronoy.parkmobiletest.data.db;

import androidx.lifecycle.LiveData;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import io.reactivex.Completable;

import java.util.List;

public class DBManager {

    private PMDatabase mDBInstance;

    public DBManager(PMDatabase db) {
        this.mDBInstance = db;
    }

    public LiveData<List<Vehicle>> getVehicles() {
        return mDBInstance.vehicleDao().getAllVehicles();
    }

    public LiveData<Vehicle> getVehicle(int vehicleId) {
        return mDBInstance.vehicleDao().getVehicleById(vehicleId);
    }

    public Completable saveVehicles(List<Vehicle> items) {
        return mDBInstance.vehicleDao().insertOrUpdate(items);
    }
}
