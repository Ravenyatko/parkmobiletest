package com.rvoronoy.parkmobiletest.data.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import io.reactivex.Completable;

import java.util.List;

@Dao
public interface VehicleDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertOrUpdate(final Vehicle item);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertOrUpdate(final List<Vehicle> item);

    @Query("DELETE FROM vehicles_table")
    void deleteAll();

    @Query("SELECT * from vehicles_table ORDER BY vrn ASC")
    LiveData<List<Vehicle>> getAllVehicles();

    @Query("SELECT * from vehicles_table where vehicleId = :vehicleId")
    LiveData<Vehicle> getVehicleById(int vehicleId);
}
