package com.rvoronoy.parkmobiletest.data.network;

import com.rvoronoy.parkmobiletest.data.network.model.VehicleResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface VehicleService {

  @GET("vehicles")
  Call<VehicleResponse> getVehiclesList();
}