package com.rvoronoy.parkmobiletest.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehicleResponse {

    @SerializedName("count")
    private int count;
    @SerializedName("vehicles")
    private List<VehicleServerModel> vehicles = null;
    @SerializedName("currentPage")
    private int currentPage;
    @SerializedName("nextPage")
    private int nextPage;
    @SerializedName("totalPages")
    private int totalPages;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<VehicleServerModel> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleServerModel> vehicles) {
        this.vehicles = vehicles;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

}