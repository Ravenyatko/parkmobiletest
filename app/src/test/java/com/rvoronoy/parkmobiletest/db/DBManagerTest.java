package com.rvoronoy.parkmobiletest.db;

import com.rvoronoy.parkmobiletest.data.db.DBManager;
import com.rvoronoy.parkmobiletest.data.db.PMDatabase;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.db.mock.MockDB;
import io.reactivex.Completable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DBManagerTest {

    private static final int ID = 123;
    private static final String VRN = "testVrn";
    private static final String COLOR = "testColor";
    private static final String COUNTRY = "testCountry";
    private static final String TYPE = "testType";
    private static final boolean DEFAULT = true;

    private DBManager mDBmanager;
    private PMDatabase mDatabase;

    @Before
    public void initDb() {
        mDatabase = new MockDB();
        mDBmanager = new DBManager(mDatabase);
    }

    @Test
    public void testSaveVehicles() {
        List<Vehicle> testData = prepareVehicleData();
        Completable completable = mDBmanager.saveVehicles(testData);
        completable.test()
                .assertSubscribed()
                .assertNoErrors()
                .assertComplete();
    }

    @Test
    public void testGetVehicles() {
        List<Vehicle> testData = prepareVehicleData();
        mDatabase.vehicleDao().insertOrUpdate(testData);
        List<Vehicle> actualData = mDBmanager.getVehicles().getValue();
        Assert.assertNotNull(actualData);
        Assert.assertEquals(actualData.size(), testData.size());
        Assert.assertEquals(actualData.get(0), testData.get(0));
    }

    private List<Vehicle> prepareVehicleData() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(ID);
        vehicle.setVrn(VRN);
        vehicle.setColor(COLOR);
        vehicle.setCountry(COUNTRY);
        vehicle.setType(TYPE);
        vehicle.setDefault(DEFAULT);

        ArrayList<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(vehicle);
        return vehicles;
    }


}
