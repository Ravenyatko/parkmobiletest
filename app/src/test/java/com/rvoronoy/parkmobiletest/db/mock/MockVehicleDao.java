package com.rvoronoy.parkmobiletest.db.mock;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.rvoronoy.parkmobiletest.data.db.VehicleDAO;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import io.reactivex.Completable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MockVehicleDao implements VehicleDAO {

    private ArrayList<Vehicle> mItems = new ArrayList<>();
    private HashMap<Integer, MockLiveData<Vehicle>> mItemLiveDataMap = new HashMap<>();
    private MutableLiveData<List<Vehicle>> mListLiveData = new MockLiveData<>();

    @Override
    public Completable insertOrUpdate(Vehicle item) {
        for (Vehicle exist : mItems) {
            if (exist.getVehicleId() == item.getVehicleId()) {
                mItems.remove(exist);
                mItems.add(item);
                mListLiveData.setValue(mItems);
                mItemLiveDataMap.get(item.getVehicleId()).postValue(item);
                return returnMockCompletable();
            }
        }
        mItems.add(item);
        mListLiveData.setValue(mItems);
        mItemLiveDataMap.put(item.getVehicleId(), new MockLiveData<>());
        mItemLiveDataMap.get(item.getVehicleId()).setValue(item);
        return returnMockCompletable();
    }

    private Completable returnMockCompletable() {
        return Completable.fromAction(() -> {
        });
    }

    @Override
    public Completable insertOrUpdate(List<Vehicle> itemList) {
        for (Vehicle iterable : itemList) {
            insertOrUpdate(iterable);
        }
        return returnMockCompletable();
    }

    @Override
    public void deleteAll() {
        mItems.clear();
        mListLiveData.setValue(mItems);
    }

    @Override
    public LiveData<List<Vehicle>> getAllVehicles() {
        return mListLiveData;
    }

    @Override
    public LiveData<Vehicle> getVehicleById(int vehicleId) {
        if (!mItemLiveDataMap.containsKey(vehicleId)) {
            mItemLiveDataMap.put(vehicleId, new MockLiveData<>());
        }
        return mItemLiveDataMap.get(vehicleId);
    }

    class MockLiveData<T> extends MutableLiveData<T> {

        T value;

        @Nullable
        @Override
        public T getValue() {
            return value;
        }

        @Override
        public void setValue(T value) {
            this.value = value;
        }
    }
}
