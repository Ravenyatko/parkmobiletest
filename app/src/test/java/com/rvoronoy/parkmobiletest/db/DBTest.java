package com.rvoronoy.parkmobiletest.db;

import android.content.Context;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import com.rvoronoy.parkmobiletest.data.db.PMDatabase;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import io.reactivex.Completable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
public class DBTest {


    private static final int ID = 123;
    private static final String VRN = "testVrn";
    private static final String COLOR = "testColor";
    private static final String COUNTRY = "testCountry";
    private static final String TYPE = "testType";
    private static final boolean DEFAULT = true;

    private PMDatabase mDatabase;
    private Context mockContext;

    @Before
    public void initDb() {
        mockContext = ApplicationProvider.getApplicationContext();
        mDatabase = Room.inMemoryDatabaseBuilder(mockContext, PMDatabase.class).allowMainThreadQueries().build();
    }

    @Test
    public void testInsert() {
        List<Vehicle> testData = prepareVehicleData();
        Completable completable = mDatabase.vehicleDao().insertOrUpdate(testData);
        completable.test()
                .assertSubscribed()
                .assertNoErrors()
                .assertComplete();
    }

    private List<Vehicle> prepareVehicleData() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(ID);
        vehicle.setVrn(VRN);
        vehicle.setColor(COLOR);
        vehicle.setCountry(COUNTRY);
        vehicle.setType(TYPE);
        vehicle.setDefault(DEFAULT);

        ArrayList<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(vehicle);
        return vehicles;
    }

}
