package com.rvoronoy.parkmobiletest.db.mock;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import com.rvoronoy.parkmobiletest.data.db.PMDatabase;
import com.rvoronoy.parkmobiletest.data.db.VehicleDAO;

public class MockDB extends PMDatabase {

    public MockDB(){
        super();
    }

    private VehicleDAO vehicleDAO = new MockVehicleDao();

    @Override
    public VehicleDAO vehicleDao() {
        return vehicleDAO;
    }

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return new SupportSQLiteOpenHelper() {
            @Override
            public String getDatabaseName() {
                return null;
            }

            @Override
            public void setWriteAheadLoggingEnabled(boolean enabled) {

            }

            @Override
            public SupportSQLiteDatabase getWritableDatabase() {
                return null;
            }

            @Override
            public SupportSQLiteDatabase getReadableDatabase() {
                return null;
            }

            @Override
            public void close() {

            }
        };
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
