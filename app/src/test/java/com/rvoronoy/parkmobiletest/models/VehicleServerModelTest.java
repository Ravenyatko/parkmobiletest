package com.rvoronoy.parkmobiletest.models;

import com.google.gson.Gson;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleServerModel;
import org.junit.Assert;
import org.junit.Test;

public class VehicleServerModelTest {

    private static final int ID = 123;
    private static final String VRN = "testVrn";
    private static final String COLOR = "testColor";
    private static final String COUNTRY = "testCountry";
    private static final String TYPE = "testType";
    private static final boolean DEFAULT = true;


    @Test
    public void testFieldsConsistency() {
        VehicleServerModel vehicle = new VehicleServerModel();
        vehicle.setVehicleId(ID);
        vehicle.setVrn(VRN);
        vehicle.setColor(COLOR);
        vehicle.setCountry(COUNTRY);
        vehicle.setType(TYPE);
        vehicle.setDefault(DEFAULT);

        Assert.assertEquals(vehicle.getVehicleId(), ID);
        Assert.assertEquals(vehicle.getVrn(), VRN);
        Assert.assertEquals(vehicle.getCountry(), COUNTRY);
        Assert.assertEquals(vehicle.getColor(), COLOR);
        Assert.assertEquals(vehicle.getType(), TYPE);
        Assert.assertEquals(vehicle.isDefault(), DEFAULT);
    }

    @Test
    public void testVehicleSerialization() {
        VehicleServerModel vehicle = new VehicleServerModel();
        vehicle.setVehicleId(ID);
        vehicle.setVrn(VRN);
        vehicle.setColor(COLOR);
        vehicle.setCountry(COUNTRY);
        vehicle.setType(TYPE);
        vehicle.setDefault(DEFAULT);

        Gson gson = new Gson();
        String json = gson.toJson(vehicle);
        Assert.assertEquals(json, "{\"vehicleId\":123,\"vrn\":\"testVrn\",\"country\":\"testCountry\",\"color\":\"testColor\",\"type\":\"testType\",\"default\":true}");
    }

    @Test
    public void testVehicleDeserialization(){
        String json = "{\"vehicleId\":123,\"vrn\":\"testVrn\",\"country\":\"testCountry\",\"color\":\"testColor\",\"type\":\"testType\",\"default\":true}";
        Gson gson = new Gson();
        VehicleServerModel vehicle = gson.fromJson(json, VehicleServerModel.class);
        Assert.assertEquals(vehicle.getVehicleId(), ID);
        Assert.assertEquals(vehicle.getVrn(), VRN);
        Assert.assertEquals(vehicle.getCountry(), COUNTRY);
        Assert.assertEquals(vehicle.getColor(), COLOR);
        Assert.assertEquals(vehicle.getType(), TYPE);
        Assert.assertEquals(vehicle.isDefault(), DEFAULT);
    }
}
