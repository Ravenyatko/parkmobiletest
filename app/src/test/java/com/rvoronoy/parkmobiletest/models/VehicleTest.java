package com.rvoronoy.parkmobiletest.models;

import com.google.gson.Gson;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleServerModel;
import org.junit.Assert;
import org.junit.Test;

public class VehicleTest {

    private static final int ID = 123;
    private static final String VRN = "testVrn";
    private static final String COLOR = "testColor";
    private static final String COUNTRY = "testCountry";
    private static final String TYPE = "testType";
    private static final boolean DEFAULT = true;


    @Test
    public void testFieldsConsistency() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(ID);
        vehicle.setVrn(VRN);
        vehicle.setColor(COLOR);
        vehicle.setCountry(COUNTRY);
        vehicle.setType(TYPE);
        vehicle.setDefault(DEFAULT);

        Assert.assertEquals(vehicle.getVehicleId(), ID);
        Assert.assertEquals(vehicle.getVrn(), VRN);
        Assert.assertEquals(vehicle.getCountry(), COUNTRY);
        Assert.assertEquals(vehicle.getColor(), COLOR);
        Assert.assertEquals(vehicle.getType(), TYPE);
        Assert.assertEquals(vehicle.isDefault(), DEFAULT);
    }

    @Test
    public void convertFromServerModelTest(){
        VehicleServerModel vehicle = new VehicleServerModel();
        vehicle.setVehicleId(ID);
        vehicle.setVrn(VRN);
        vehicle.setColor(COLOR);
        vehicle.setCountry(COUNTRY);
        vehicle.setType(TYPE);
        vehicle.setDefault(DEFAULT);

        Vehicle testModel = new Vehicle(vehicle);
        Assert.assertEquals(testModel.getVehicleId(), ID);
        Assert.assertEquals(testModel.getVrn(), VRN);
        Assert.assertEquals(testModel.getCountry(), COUNTRY);
        Assert.assertEquals(testModel.getColor(), COLOR);
        Assert.assertEquals(testModel.getType(), TYPE);
        Assert.assertEquals(testModel.isDefault(), DEFAULT);
    }
}
