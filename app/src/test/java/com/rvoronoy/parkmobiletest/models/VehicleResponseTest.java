package com.rvoronoy.parkmobiletest.models;

import com.google.gson.Gson;
import com.rvoronoy.parkmobiletest.data.db.model.Vehicle;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleResponse;
import com.rvoronoy.parkmobiletest.data.network.model.VehicleServerModel;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class VehicleResponseTest {

    private static final int COUNT = 123;
    private static final int CURRENT_PAGE = 12;
    private static final int NEXT_PAGE = 13;
    private static final int TOTAL_PAGES = 130;
    private static final String TYPE = "testType";
    private static final boolean DEFAULT = true;


    @Test
    public void testFieldsConsistency() {
        VehicleResponse response = new VehicleResponse();
        response.setCount(COUNT);
        response.setCurrentPage(CURRENT_PAGE);
        response.setNextPage(NEXT_PAGE);
        response.setTotalPages(TOTAL_PAGES);

        VehicleServerModel vehicle = new VehicleServerModel();
        ArrayList<VehicleServerModel> items = new ArrayList<>();
        items.add(vehicle);

        response.setVehicles(items);

        Assert.assertEquals(response.getCount(), COUNT);
        Assert.assertEquals(response.getCurrentPage(), CURRENT_PAGE);
        Assert.assertEquals(response.getNextPage(), NEXT_PAGE);
        Assert.assertEquals(response.getTotalPages(), TOTAL_PAGES);
        Assert.assertEquals(response.getVehicles().size(), 1);
        Assert.assertEquals(response.getVehicles().get(0), vehicle);
    }

    @Test
    public void testVehicleSerialization() {
        VehicleResponse response = new VehicleResponse();
        response.setCount(COUNT);
        response.setCurrentPage(CURRENT_PAGE);
        response.setNextPage(NEXT_PAGE);
        response.setTotalPages(TOTAL_PAGES);

        VehicleServerModel vehicle = new VehicleServerModel();
        ArrayList<VehicleServerModel> items = new ArrayList<>();
        items.add(vehicle);

        response.setVehicles(items);

        Gson gson = new Gson();
        String json = gson.toJson(response);
        Assert.assertEquals(json, "{\"count\":123,\"vehicles\":[{\"vehicleId\":0,\"default\":false}],\"currentPage\":12,\"nextPage\":13,\"totalPages\":130}");
    }

    @Test
    public void testVehicleDeserialization(){
        String json = "{\"count\":123,\"vehicles\":[{\"vehicleId\":0,\"default\":false}],\"currentPage\":12,\"nextPage\":13,\"totalPages\":130}";
        Gson gson = new Gson();
        VehicleResponse response = gson.fromJson(json, VehicleResponse.class);
        Assert.assertEquals(response.getCount(), COUNT);
        Assert.assertEquals(response.getCurrentPage(), CURRENT_PAGE);
        Assert.assertEquals(response.getNextPage(), NEXT_PAGE);
        Assert.assertEquals(response.getTotalPages(), TOTAL_PAGES);
        Assert.assertEquals(response.getVehicles().size(), 1);
    }
}
